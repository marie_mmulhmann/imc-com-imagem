package com.example.imc;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivityIMC extends AppCompatActivity {


    EditText peso, altura;
    TextView resultadoIMC;
    ImageView ResultadoImagem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void CalcularIMC(View v){

        peso = findViewById(R.id.pesoText);
        altura = findViewById(R.id.alturaText);
        resultadoIMC = findViewById(R.id.resultadoView);
        ResultadoImagem = findViewById(R.id.imageViewIMC);

        String PesoP = peso.getText().toString();
        String AlturaA = altura.getText().toString();

        float finalPeso = Float.parseFloat(PesoP);
        float finalAltura = Float.parseFloat(AlturaA);

        float IMC = (finalPeso/(finalAltura*finalAltura));

        String ResultIMC = String.valueOf(IMC);

        resultadoIMC.setText(ResultIMC);


            if(IMC>=39){
                Toast toast = Toast.makeText(getApplicationContext(), "Obesidade Morbida", Toast.LENGTH_LONG);
                toast.show();
                Drawable drawable= getResources().getDrawable(R.drawable.img1);
                ResultadoImagem.setImageDrawable(drawable);
            }else if((IMC>=29||IMC<=38.9)){
                Toast.makeText(getApplicationContext(), "Obesidade Moderada", Toast.LENGTH_LONG).show();
                Drawable drawable= getResources().getDrawable(R.drawable.img3);
                ResultadoImagem.setImageDrawable(drawable);
            }else if((IMC>=24||IMC<=28.9)){
                Toast.makeText(getApplicationContext(), "Obesidade Leve", Toast.LENGTH_LONG).show();
                Drawable drawable= getResources().getDrawable(R.drawable.img2);
                ResultadoImagem.setImageDrawable(drawable);
            }else if((IMC>=19||IMC<=23.9)){
                Toast.makeText(getApplicationContext(), "Normal", Toast.LENGTH_LONG).show();
                Drawable drawable= getResources().getDrawable(R.drawable.img5);
                ResultadoImagem.setImageDrawable(drawable);
            }else if(IMC<19){
                Toast.makeText(getApplicationContext(), "Abaixo do Normal", Toast.LENGTH_LONG).show();
                Drawable drawable= getResources().getDrawable(R.drawable.img4);
                ResultadoImagem.setImageDrawable(drawable);
            }

    }

}
